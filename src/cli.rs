use clap::Parser;

#[derive(Parser)]
/// Watch for changes in directories
pub struct Cli {
    /// Directory to watch
    pub directory: String,
}
