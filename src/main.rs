mod cli;

use walkdir::WalkDir;
use clap::Parser;
use colored::Colorize;
use rayon::prelude::*;

fn main() {
    let args = cli::Cli::parse();

    let mut items_old = recursive_list_directory(args.directory.as_str());

    loop {
        let items = recursive_list_directory(args.directory.as_str());

        items.par_iter().for_each(|i| {
            if items_old.contains(i) == false {
                println!("{}", format!("+ {}", i).bright_green().bold());
            }
        });

        items_old.par_iter().for_each(|i| {
            if items.contains(i) == false {
                println!("{}", format!("- {}", i).bright_red().bold());
            }
        });

        // TODO: Add file changing detection.

        items_old = items;
    }
}

fn recursive_list_directory(dir: &str) -> Vec<String> {
    let mut items: Vec<String> = Vec::new();

    for i in WalkDir::new(dir) {
        match &i {
            Ok(o) => items.push(o.path().display().to_string()),
            Err(_) => (),
        };
    }

    return items;
}
